import { Component } from "react";

class FetchApi extends Component {
    fetchAPI = async (url, requestOptions) => {
        let res = await fetch(url, requestOptions);
        let data = await res.json()
        return data
    }

    fetchAPIDrink = async (urlDrink, requestOptionsDrink) => {
        let resDink = await fetch(urlDrink, requestOptionsDrink);
        let dataDrink = await resDink.json()
        return dataDrink
    }

    fetchAPIVoucher = async (urlVoucher, requestOptionsVoucher) => {
        let resVoucher = await fetch(urlVoucher, requestOptionsVoucher);
        let dataVoucher = await resVoucher.json()
        return dataVoucher
    }



    getByIdhandler = () => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };

        var url = "http://203.171.20.210:8080/devcamp-pizza365/orders/I1zm3z6eHL"

        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response) // bắt được return
            })
            .catch((err) => {
                console.error(err)
            })


    }

    getAllHandler = () => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };

        var url = "http://203.171.20.210:8080/devcamp-pizza365/orders"

        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response) // bắt được return
            })
            .catch((err) => {
                console.error(err)
            })
    }

    postHandler = () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "id": 123456,
            "orderCode": "test",
            "kichCo": null,
            "duongKinh": "",
            "suon": 0,
            "salad": "",
            "loaiPizza": "Hawai",
            "idVourcher": "",
            "thanhTien": 0,
            "giamGia": 0,
            "idLoaiNuocUong": null,
            "soLuongNuoc": 0,
            "hoTen": null,
            "email": "anh@gmail.com",
            "soDienThoai": null,
            "diaChi": null,
            "loiNhan": null,
            "trangThai": "open",
            "ngayTao": 1677084658363,
            "ngayCapNhat": 1677084658363
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        var url = "http://203.171.20.210:8080/devcamp-pizza365/orders"

        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response) // bắt được return
            })
            .catch((err) => {
                console.error(err)
            })

    }

    updaterHandler = () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        
        var raw = JSON.stringify({
          "id": 123456,
          "orderCode": "test",
          "kichCo": "fgjkldfgkld",
          "duongKinh": "test",
          "suon": 0,
          "salad": "",
          "loaiPizza": "Hawai",
          "idVourcher": "",
          "thanhTien": 0,
          "giamGia": 0,
          "idLoaiNuocUong": null,
          "soLuongNuoc": 0,
          "hoTen": null,
          "email": "anh@gmail.com",
          "soDienThoai": null,
          "diaChi": null,
          "loiNhan": null,
          "trangThai": "open",
          "ngayTao": 1677084658363,
          "ngayCapNhat": 1677084658363
        });
        
        var requestOptions = {
          method: 'PUT',
          headers: myHeaders,
          body: raw,
          redirect: 'follow'
        };
        
        
        var url = "http://203.171.20.210:8080/devcamp-pizza365/orders/GhQIXJadXg"

        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response) // bắt được return
            })
            .catch((err) => {
                console.error(err)
            })
    }

    checkVouCher=()=>{
        var requestOptionsVoucher = {
            method: 'GET',
            redirect: 'follow'
          };
          
        var urlVoucher = "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/24864"
        this.fetchAPIVoucher(urlVoucher, requestOptionsVoucher)
        .then((response) => {
            console.log(response) // bắt được return
        })
        .catch((err) => {
            console.error(err)
        })
    }
    
    getDrik =()=>{
        var requestOptionsDrink = {
            method: 'GET',
            redirect: 'follow'
          };
          
          var urlDrink = "http://203.171.20.210:8080/devcamp-pizza365/drinks";

          this.fetchAPIDrink(urlDrink, requestOptionsDrink)
            .then((response) => {
                console.log(response) // bắt được return
            })
            .catch((err) => {
                console.error(err)
            })


    }
    render() {
        return (
            <>
                <div className="row">
                    <h4>Fetch API</h4>
                </div>
                <div className="justify-content">
                    <div className="form-control mt-3 bg-warning">
                        <button className="btn btn-danger" onClick={this.getAllHandler}>Get all</button>
                    </div>
                    <div className="form-control mt-3 bg-warning">
                        <button className="btn btn-danger" onClick={this.getByIdhandler}>Get detail</button>
                    </div>
                    <div className="form-control mt-3 bg-warning">
                        <button className="btn btn-danger" onClick={this.postHandler}>Create</button>
                    </div>
                    <div className="form-control mt-3 bg-warning">
                        <button className="btn btn-danger" onClick={this.updaterHandler}>Update</button>
                    </div>
                    <div className="form-control mt-3 bg-warning">
                        <button className="btn btn-danger" onClick={this.checkVouCher}>check voucher by id</button>
                    </div>
                    <div className="form-control mt-3 bg-warning">
                        <button className="btn btn-danger" onClick={this.getDrik}>Get drink list</button>
                    </div>
                </div>
            </>
        )
    }
}

export default FetchApi
