import "bootstrap/dist/css/bootstrap.min.css";
import './App.css';
import FetchApi from './component/fetch';

function App() {
  return (
    <div className="container text-center mt-5">
    <div className="row justify-content-center">
      <div className="col-6">
        <FetchApi></FetchApi>
      </div>
    </div>
  </div>
  )
}

export default App;
